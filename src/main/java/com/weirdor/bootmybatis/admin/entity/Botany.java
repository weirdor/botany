package com.weirdor.bootmybatis.admin.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.activerecord.Model;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author weirdor
 * @since 2018-03-07
 */
public class Botany extends Model<Botany> {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
	private Long id;
    /**
     * 纲
     */
	private String gang;
    /**
     * 名称
     */
	private String name;
    /**
     * 图片
     */
	private String picture;
    /**
     * 描述
     */
	private String description;
    /**
     * 操作人
     */
	private String user;
    /**
     * 目
     */
	private String mu;
    /**
     * 科
     */
	private String ke;
    /**
     * 属
     */
	private String shu;


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getGang() {
		return gang;
	}

	public void setGang(String gang) {
		this.gang = gang;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getMu() {
		return mu;
	}

	public void setMu(String mu) {
		this.mu = mu;
	}

	public String getKe() {
		return ke;
	}

	public void setKe(String ke) {
		this.ke = ke;
	}

	public String getShu() {
		return shu;
	}

	public void setShu(String shu) {
		this.shu = shu;
	}

	@Override
	protected Serializable pkVal() {
		return this.id;
	}

	@Override
	public String toString() {
		return "Botany{" +
			"id=" + id +
			", gang=" + gang +
			", name=" + name +
			", picture=" + picture +
			", description=" + description +
			", user=" + user +
			", mu=" + mu +
			", ke=" + ke +
			", shu=" + shu +
			"}";
	}
}
