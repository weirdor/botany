package com.weirdor.bootmybatis.admin.mapper;

import com.weirdor.bootmybatis.admin.entity.Botany;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author weirdor
 * @since 2018-03-07
 */
public interface BotanyMapper extends BaseMapper<Botany> {

}
