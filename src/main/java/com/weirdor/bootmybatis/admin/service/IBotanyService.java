package com.weirdor.bootmybatis.admin.service;

import com.weirdor.bootmybatis.admin.entity.Botany;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author weirdor
 * @since 2018-03-07
 */
public interface IBotanyService extends IService<Botany> {

}
