package com.weirdor.bootmybatis.admin.service.impl;

import com.weirdor.bootmybatis.admin.entity.Botany;
import com.weirdor.bootmybatis.admin.mapper.BotanyMapper;
import com.weirdor.bootmybatis.admin.service.IBotanyService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author weirdor
 * @since 2018-03-07
 */
@Service
public class BotanyServiceImpl extends ServiceImpl<BotanyMapper, Botany> implements IBotanyService {

}
