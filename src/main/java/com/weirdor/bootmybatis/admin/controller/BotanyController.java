package com.weirdor.bootmybatis.admin.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.weirdor.bootmybatis.admin.entity.Botany;
import com.weirdor.bootmybatis.admin.service.IBotanyService;
import com.weirdor.bootmybatis.admin.utils.R;
import com.xiaoleilu.hutool.convert.Convert;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author weirdor
 * @since 2018-03-07
 */
@RestController
@RequestMapping("/api")
public class BotanyController {

    @Autowired
    private IBotanyService botanyService;

    @RequestMapping(value = "/botany", method = RequestMethod.GET)
    public R getBotanyList(@RequestParam Map<String, Object> params) {
        // 处理"/Botanys/"的GET请求，用来获取用户列表
        // 还可以通过@RequestParam从页面中传递参数来进行查询条件或者翻页信息的传递
        EntityWrapper<Botany> wrapper=new EntityWrapper<>();
        if (params.containsKey("leimu")){
            if (params.get("leimu").toString().equals("1")) {
                wrapper.like("gang", params.get("name").toString());
            }
            if (params.get("leimu").toString().equals("2")) {
                wrapper.like("mu", params.get("name").toString());
            }
            if (params.get("leimu").toString().equals("3")) {
                wrapper.like("ke", params.get("name").toString());
            }
            if (params.get("leimu").toString().equals("4")) {
                wrapper.like("shu", params.get("name").toString());
            }
            if (params.get("leimu").toString().equals("5")) {
                wrapper.like("name", params.get("name").toString());
            }
        }
        Page<Botany> pageUtil = botanyService.selectPage(new Page<>(Convert.toInt(params.get("page")), Convert.toInt(params.get("limit"))), wrapper);
        return R.ok().put("data",pageUtil.getRecords());
    }
    @RequestMapping(value = "/botany", method = RequestMethod.POST)
    public R postBotany(@ModelAttribute Botany Botany) {
        // 处理"/Botanys/"的POST请求，用来创建Botany
        // 除了@ModelAttribute绑定参数之外，还可以通过@RequestParam从页面中传递参数
        botanyService.insert(Botany);
        return  R.ok("新增成功");
    }

    @RequestMapping(value = "/botany/{id}", method = RequestMethod.GET)
    public R getBotany(@PathVariable Long id) {
        // 处理"/Botanys/{id}"的GET请求，用来获取url中id值的Botany信息
        // url中的id可通过@PathVariable绑定到函数的参数中
        Botany Botany = botanyService.selectById(id);
        return R.ok("查询成功").put("Botany",Botany);
    }

    @RequestMapping(value = "/botany/{id}", method = RequestMethod.PUT)
    public R putBotany(@PathVariable Long id, @ModelAttribute Botany Botany) {
        // 处理"/Botanys/{id}"的PUT请求，用来更新Botany信息
        Botany.setId(id);
        EntityWrapper<Botany> wrapper=new EntityWrapper<>();
        botanyService.update(Botany,wrapper);
        return R.ok( "更新成功");
    }

    // 处理"/Botanys/{id}"的DELETE请求，用来删除Botany
    @RequestMapping(value = "/botany/{id}", method = RequestMethod.DELETE)
    public R deleteBotany(@PathVariable Long id) {
        botanyService.deleteById(id);
        return R.ok( "删除成功");
    }
}

